<?php 
/*
Template Name: Home
*/
get_header();?> 
   <!--Banner Start-->
  
    <div class="container"> 
    <div class="banner">
      <!--Header Start-->
     
    <div class="caption text-right">
    <?php
        $args = array(
          'post_type' => 'page',
          'p'=>9
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
        <h2><?php the_title();?></h2>
        <h3><?php the_content();?></h3>
        <?php   endwhile; 
        wp_reset_query(); 
        ?>
      </div>
    </div>
  </div>
  <!--Banner End--> 
<!--Service Section Start-->
  <div class="service-section">
    <div class="container">
      <div class="row">
        <?php
     $args = array(
     'posts_per_page' => 4,
     'post_type' => 'service'              
     );
   $counter = 0;
   query_posts($args);
   global $wp_query;
   $count_post = $wp_query->post_count;
   while (have_posts()) : the_post();
   $counter++;
   ?>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
            <div class="flipper box-wrapper effect2 text-center ">
              <div class="front"> 
                <!-- front content -->
                <article> <a href="<?php the_permalink();?>"><i class="fa fa-paint-brush"></i></a>
                  <h4><?php the_title();?></h4>
                  <p><?php echo substr(strip_tags(get_the_excerpt()),0,90);?>...</p>
                  <a href="<?php the_permalink();?>" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
              <div class="back"> 
                <!-- back content -->
                <article> <a href="<?php the_permalink();?>"><i class="fa fa-paint-brush"></i></a>
                  <h4><?php the_title();?></h4>
                  <p><?php echo substr(strip_tags(get_the_excerpt()),0,90);?>...</p>
                  <a href="<?php the_permalink();?>" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> </article>
              </div>
            </div>
          </div>
        </div>
         <?php   endwhile;  
 wp_reset_query();
     ?> 
      </div>
    </div>
  </div>
  <!--Service Section End--> 
  <!--Intro Text start-->
  <div class="container">
  <section class="welcome-text">
    <div class="left-half hidden-xs"> </div>
    <div class="right-half">
      <article>
      <?php
        $args = array(
          'post_type' => 'page',
          'p'=>16
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
        <h2><?php echo get_the_excerpt(16);?></h2>
        <?php the_content();?>
        <a href="<?php echo get_permalink(16);?>" class="btn btn-warning">Read More <i class="fa fa-chevron-circle-right"></i></a> 
          <?php   endwhile;  
 wp_reset_query();
     ?> 
        </article>
    </div>
  </section>
  </div>
  <!--Intro Text End--> 
  <!--Why Choose Us Start-->
  <section class="why-choose">
    <div class="container">
      <div class="title">
        <h3>Why Choose Us?</h3>
        <p class="lead">Specialty media that expand your capabilities. Workflow solutions
          that streamline your processes.</p>
      </div>
      <div class="row">
       <?php
     $args = array(
     'posts_per_page' => -1,
     'post_type' => 'whyus'              
     );
   $counter = 0;
   query_posts($args);
   global $wp_query;
   $count_post = $wp_query->post_count;
   while (have_posts()) : the_post();
   $counter++;
   ?>
        <div class="col-md-6 col-xs-12">
          <div class="choose-list"> <i class="fa fa-hand-o-right"></i>
            <h4><?php the_title();?></h4>
             <?php the_content(); ?>
          </div>
        </div>
        <?php   endwhile;  
 wp_reset_query();
     ?> 
      </div>
    </div>
  </section>
  <!--Why Choose Us Start-->
      <div class="container">
  <section class="quotation parallax text-center hidden-xs hidden-sm">

    <div class="row"><div class="col-md-8 col-md-offset-2">
<?php
        $args = array(
          'post_type' => 'page',
          'p'=>20
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
    <h2><?php the_title();?></h2>
      <p><?php the_content(); ?></p>
     <?php   endwhile;  
 wp_reset_query();
     ?> 
      </div></div>

  </section>
    </div>
  <!--Our Work Start-->
  <section class="our-work">
    <div class="container">
      <div class="title">
        <h3>Our Work</h3>
        <p class="lead">Specialty media that expand your capabilities. Workflow solutions
          that streamline your processes.</p>
      </div>
      <!-- isotope filters start -->
       <?php 
               $args = array(
              'type'                     => 'work',
              'orderby'                  => 'id',
              'order'                    => 'ASC',
              'hide_empty'               => 1,
              'hierarchical'             => 1,
              'taxonomy'                 => 'workcat',
              'hide_empty'               => 0,
              'pad_counts'               => false );
               ?>
      <div class="filters">
        <ul class="nav nav-pills">
         <li class="active"><a href="#" data-filter="*">All Designs</a></li>               
               <?php $categories = get_categories( $args );
              //print_r($categories);
              $i=1;
               foreach($categories as $cat_obj){
              ?>
          <li><a href="#" data-filter=".<?php echo $cat_obj->slug;?>">
          <?php echo $cat_obj->name;?></a></li>
          <?php  $i++; } ?>
        </ul>
      </div>
      <!-- isotope filters end -->
      <?php 
               $args = array(
              'type'                     => 'work',
              'orderby'                  => 'id',
              'order'                    => 'ASC',
              'hide_empty'               => 1,
              'hierarchical'             => 1,
              'taxonomy'                 => 'workcat',
              'hide_empty'               => 0,
              'pad_counts'               => false );
               ?>
      <div class="isotope-container row grid-space-10">
      <?php $categories = get_categories( $args );
              //print_r($categories);
              $i=1;
               foreach($categories as $cat_obj){
              ?>
      <?php
                               $args = array(
          'posts_per_page' => -1,
          'post_type' => 'work',
          'orderby' => 'menu_order',
          'order' => 'ASC',
          'tax_query' => array(
            array(
              'taxonomy' => 'workcat',
              'field' => 'slug',
              'terms' => $cat_obj->slug
            )
          )
        );
        $counter = 0;
        query_posts($args);
        while (have_posts()) : the_post();
          $counter++;
                ?>
        <div class="col-sm-6 col-md-4 isotope-item <?php echo $cat_obj->slug;?>">
          <div class="box-style-1 white-bg">
          <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full size');
          ?>
            <div class="overlay-container"> <img src="<?php echo $image[0];?>" alt="<?php the_title();?>"> <a href="<?php echo $image[0];?>" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
            <?php }?>
          </div>
        </div>
        <?php   endwhile;  
 wp_reset_query();
 $i++;
  }
     ?> 
      </div>

    </div>
  </section>
  <!--Our Work End--> 
  <?php get_footer();?>