<?php 
/*=======================================================================*/
// Facility  Post Type
/*=======================================================================*/
add_action('init', 'register_whyus');
function register_whyus(){
	$labels = array(
		'name' => _x('Why Us', 'post type general name'),
		'singular_name' => _x('Why Us', 'post type singular name'),
		'add_new' => _x('Add New', 'Why Us'),
		'add_new_item' => __('Why Us'),
		'edit_item' => __('Edit Why Us'),
		'new_item' => __('New Why Us'),
		'view_item' => __('View Why Us'),
		'search_items' => __('Search Why Us'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''

					);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/images/slider-icon.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		//'menu_position' => '',
		'supports' => array('title', 'editor', 'thumbnail','excerpt')
				);
	register_post_type('whyus' , $args);

}



?>