<?php 
add_action('add_meta_boxes', 'register_meta_box');
add_action('save_post', 'save_meta_box');

function register_meta_box(){

	add_meta_box( 'designation', __( 'Designations', 'Designations' ), 'designations_display_callback', 'team' );
	add_meta_box( 'phone', __( 'Phone', 'Phone' ), 'phone_display_callback', 'team' );
	add_meta_box( 'email', __( 'Email', 'Email' ), 'email_display_callback', 'team' );

    }

 function designations_display_callback($post) 
    {
        $designations = get_post_meta( $post->ID, 'designations', true );

        $outline = '';

        //$outline .= '<label for="country">'. __('Country', 'wp') .'</label>';
        $outline .= '<input type="text" name="designations" id="designations" value="'. esc_attr($designations) .'" />';
     
        echo $outline;
    }

    function phone_display_callback($post) 
    {
        $phone = get_post_meta( $post->ID, 'phone', true );

        $outline = '';

        //$outline .= '<label for="country">'. __('Country', 'wp') .'</label>';
        $outline .= '<input type="text" name="phone" id="phone" value="'. esc_attr($phone) .'" />';
     
        echo $outline;
    }

     function email_display_callback($post) 
    {
        $email = get_post_meta( $post->ID, 'email', true );

        $outline = '';

        //$outline .= '<label for="country">'. __('Country', 'wp') .'</label>';
        $outline .= '<input type="text" name="email" id="email" value="'. esc_attr($email) .'" />';
     
        echo $outline;
    }

    function save_meta_box( $post_id)
    {
    	 $designations   = isset( $_POST['designations'] ) ? $_POST['designations'] : '';
        update_post_meta($post_id,'designations',$designations);

         $phone   = isset( $_POST['phone'] ) ? $_POST['phone'] : '';
        update_post_meta($post_id,'phone',$phone);

         $email   = isset( $_POST['email'] ) ? $_POST['email'] : '';
        update_post_meta($post_id,'email',$email);
    }