<?php 
/*=======================================================================*/
// Team Post Type
/*=======================================================================*/
add_action('init', 'register_team');
function register_team(){
	$labels = array(
		'name' => _x('Team', 'post type general name'),
		'singular_name' => _x('Team', 'post type singular name'),
		'add_new' => _x('Add New', 'Team'),
		'add_new_item' => __('Team'),
		'edit_item' => __('Edit Team'),
		'new_item' => __('New Team'),
		'view_item' => __('View Team'),
		'search_items' => __('Search Team'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''

					);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/images/slider-icon.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		//'menu_position' => '',
		'supports' => array('title', 'editor', 'thumbnail','excerpt')
				);
	register_post_type('team' , $args);

}



?>