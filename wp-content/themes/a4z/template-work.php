<?php 
/*
Template Name: Work
*/
get_header();?>
      <div class="caption text-center padding-20">
        <h2>Our Work</h2>
        <ol class="breadcrumb pull-right">
          <li><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="active">Our Work</li>
        </ol>
      </div>
    </div>
  </div>
  <!--Inner page content-->
  <section class="our-work padding-20">
    <div class="container">
      <div class="title">
        <h3>Our Work</h3>
        <p class="lead">Specialty media that expand your capabilities. Workflow solutions
          that streamline your processes.</p>
      </div>
      <!-- isotope filters start -->
      <div class="filters">
        <ul class="nav nav-pills">
         <?php 
               $args = array(
              'type'                     => 'work',
              'orderby'                  => 'id',
              'order'                    => 'ASC',
              'hide_empty'               => 1,
              'hierarchical'             => 1,
              'taxonomy'                 => 'workcat',
              'hide_empty'               => 0,
              'pad_counts'               => false );
               ?>
          <li class="active"><a href="#" data-filter="*">All Designs</a></li>
          <?php $categories = get_categories( $args );
              // print_r($categories);
              $i=1;
               foreach($categories as $cat_obj){
              ?>
          <li><a href="#" data-filter=".<?php echo $cat_obj->slug;?>"><?php echo $cat_obj->name;?></a></li>
          <?php $i++;} ?>
        </ul>
      </div>
      <!-- isotope filters end -->
      <?php 
               $args = array(
              'type'                     => 'work',
              'orderby'                  => 'id',
              'order'                    => 'ASC',
              'hide_empty'               => 1,
              'hierarchical'             => 1,
              'taxonomy'                 => 'workcat',
              'hide_empty'               => 0,
              'pad_counts'               => false );
               ?>
      <div class="isotope-container row grid-space-10">
      <?php $categories = get_categories( $args );
              //print_r($categories);
              $i=1;
               foreach($categories as $cat_obj){
              ?>
      <?php
                               $args = array(
          'posts_per_page' => -1,
          'post_type' => 'work',
          'orderby' => 'menu_order',
          'order' => 'ASC',
          'tax_query' => array(
            array(
              'taxonomy' => 'workcat',
              'field' => 'slug',
              'terms' => $cat_obj->slug
            )
          )
        );
        $counter = 0;
        query_posts($args);
        while (have_posts()) : the_post();
          $counter++;
                ?>
        <div class="col-sm-6 col-md-4 isotope-item <?php echo $cat_obj->slug;?>">
          <div class="box-style-1 white-bg">
           <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full size');
          ?>
            <div class="overlay-container"> <img src="<?php echo $image[0];?>" alt="<?php the_title();?>"> <a href="<?php echo $image[0];?>" class="overlay small popup-img"> <i class="fa fa-search-plus"></i> </a> </div>
             <?php }?>
          </div>
        </div>
        <?php   endwhile;  
 wp_reset_query();
 $i++;
  }
     ?> 
        
        
        
        
      </div>
    </div>
  </section>
  <!--Inner page content-->
 <?php get_footer(); ?>
