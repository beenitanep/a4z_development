 <!--footer start-->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-xs-6">
        <?php
        $args = array(
          'post_type' => 'page',
          'p'=>16
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
          <h2>About Company</h2>
          <span class="footer-logo"> <img src="<?php bloginfo('template_url');?>/img/footer-logo.png" alt=""> </span>
          <p><?php echo substr(strip_tags(get_the_content()),0,105);?>...</p>
          <a href="<?php the_permalink();?>">Read More <i class="fa fa-long-arrow-right"></i></a>
            <?php   endwhile;  
 wp_reset_query();
     ?> 
           </div>
        <div class="col-md-3 col-xs-6">
          <h2>Useful links</h2>
          <ul>
            <?php 
               wp_nav_menu( array (
                            'theme_location'    => 'secondary',
                            'container'         => '',
                            //'menu_id'           => 'top-nav',
                            'depth'             => 0, // set to 1 to disable dropdowns
                            'fallback_cb'       => false,
                            //'menu_class'    => 'nav navbar-nav'
                            //'after'   => '|'
                        ));
          ?>
          </ul>
        </div>
        <div class="col-md-3 col-xs-6">
          <h2>Follow Us</h2>
          <ul class="social-media">
            <li><a href="#"><i class="fa fa-facebook"></i> </a>Find us on facebook</li>
            <li><a href="#"><i class="fa fa-twitter"></i> </a>Follow us on twitter</li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a>Link on linkedin</li>
          </ul>
        </div>
        <div class="col-md-3 col-xs-6">
          <div class="sitemap">
            <h2>Get In Touch</h2>
            <ul class="footer-contact">
              <li><i class="fa fa-map-marker"></i>Newroad, Pokhara, Nepal</li>
              <li><i class="fa fa-phone"></i>+997-61-522751</li>
              <li><i class="fa fa-envelope"></i><a href="#">a4zgdpp@gmail.com</a></li>
              <li><i class="fa fa-globe"></i><a href="#">www.a4zgdpp.com.np</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="bottom-footer"> <small>&copy; 2016. All Right Reserved A4Z Printing Press</small> <small class="text-right">Designed By <a href="http://webpagenepal.com/" target="_blank">Webpage Nepal</a></small> </div>
    </div>
  </footer>
  <!--footer end--> 
</div> 
 <?php wp_footer(); ?>
</body>
</html>