<?php 
/*
Template Name: About
*/
get_header();?> 
      <div class="caption text-center padding-20">
        <h2>About Us</h2>
        <ol class="breadcrumb pull-right">
          <li><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="active">About Us</li>
        </ol>
      </div>
    </div>
  </div>
  <!--Inner page content-->
  <section class="inner-content padding-20">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
        <?php
    if (have_posts()) : while (have_posts()) : the_post();
    ?>
          <div class="title">
            <h2><?php echo get_the_excerpt(16);?></h2>
          </div>
          <?php the_content();?>
         <?php endwhile; 
endif; ?> 
        </div>
        <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'about');
          ?>
        <div class="col-md-6"> <img src="<?php echo $image[0];?>" alt="<?php the_title();?>"> </div>
        <?php }?>
      </div>
      <hr/>
      <div class="team padding-20">
        <div class="title">
          <h2>Our Team</h2>
          <p class="lead">Meet Our Team</p>
        </div>
        <?php
     $args = array(
     'posts_per_page' => -1,
     'post_type' => 'team'              
     );
   $counter = 0;
   query_posts($args);
   global $wp_query;
   $count_post = $wp_query->post_count;
   while (have_posts()) : the_post();
   $counter++;
   ?>
        <div class="item-hover circle effect7 left_to_right"><a href="#">
        <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'team');
          ?>
          <div class="img"><img src="<?php echo $image[0];?>" alt="<?php the_title();?>"></div>
          <?php }?>
          <div class="info">
          <?php 
                   $designations = get_post_meta( $post->ID, 'designations', true );
             if(  $designations!= ''){?>
          <h2><?php echo $designations;?></h2>
                <?php } 
                ?>
            <h3><?php the_title();?></h3>
            <?php 
             $phone = get_post_meta( $post->ID, 'phone', true );
             if(  $phone!= ''){
            ?>
            <p><?php echo $phone;?></p>
            <?php } 
              $email = get_post_meta( $post->ID, 'email', true );
             if(  $email!= ''){
            ?>
            <p><?php echo $email;?></p>
            <?php } ?>
          </div>
          </a></div>
          <?php   endwhile;  
 wp_reset_query();
     ?> 
      </div>
    </div>
  </section>
  <!--Inner page content-->
<?php get_footer();?>
