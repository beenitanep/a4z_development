<?php 
/*
Template Name: Gallery
*/
get_header();?> 
 <div class="caption text-center padding-20">
        <h2>Gallery</h2>
        <ol class="breadcrumb pull-right">
          <li><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="active">Gallery</li>
        </ol>
      </div>
    </div>
  </div>
  <!--Inner page content-->
  <section class="inner-content padding-20">
    <div class="container">
      <!-- Masonry wrapper -->
                    	<div class="wp-masonry-wrapper wp-masonry-3-cols">
                        <div class="wp-masonry-gutter"></div>
                          <?php
     $args = array(
     'posts_per_page' => -1,
     'post_type' => 'gallery'              
     );
   $counter = 0;
   query_posts($args);
   global $wp_query;
   $count_post = $wp_query->post_count;
   while (have_posts()) : the_post();
   $counter++;
   ?>
                        <div class="wp-masonry-block">
                          <div class="post-item style1">
                            <div class="post-content-wr">
                                <div class="post-meta-top">
                                 <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full size');
          ?>
                                    <div class="post-image">
                                      <a href="<?php echo $image[0];?>" class="popup-img">
                                                                                  <img src="<?php echo $image[0];?>" alt="<?php the_title();?>">
                                                                              </a>
                                    </div>
                                    	<?php }?>
                                </div>
                                
                                
                            </div>
                          </div>
                        </div>
                        
                        <?php   endwhile;  
 wp_reset_query();
     ?> 
                      </div>
     
      
    </div>
  </section>
  <!--Inner page content-->
<?php get_footer();?>
