<?php get_header();?> 
      <div class="caption text-center padding-20">
        <h2><?php the_title();?></h2>
        <ol class="breadcrumb pull-right">
          <li><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="active"><?php the_title();?></li>
        </ol>
      </div>
    </div>
  </div>
  <!--Inner page content-->
  <section class="inner-content padding-20">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
        <?php
    if (have_posts()) : while (have_posts()) : the_post();
    ?> 
          <?php the_content();?>
         <?php endwhile; 
endif; ?> 
        </div>
        <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'about');
          ?>
        <div class="col-md-6"> <img src="<?php echo $image[0];?>" alt="<?php the_title();?>"> </div>
        <?php }?>
      </div>
    </div>
  </section>
  <!--Inner page content-->
<?php get_footer();?>
