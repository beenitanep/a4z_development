<?php 
/*
Template Name: Contact Us
*/
get_header();?>
      <div class="caption text-center padding-20">
        <h2>Contact Us</h2>
        <ol class="breadcrumb pull-right">
          <li><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="active">Contact Us</li>
        </ol>
      </div>
    </div>
  </div>
  <!--Inner page content-->
  <!-- Importing slider content -->
  <section class="slice">
        <div class="wp-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="section-title-wr">
                            <h4 class="section-title left"><span>Send us a message</span></h4>
                        </div>
                        <p><?php echo get_the_excerpt(62);?> </p>
                        <?php echo do_shortcode( '[contact-form-7 id="61" title="Contact Us" html_class="form-light mt-20"]' ); ?>
                    </div>
                    
                    <div class="col-md-5">
                        <div id="mapCanvas" class="map-canvas no-margin"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3515.6816738374378!2d83.98408141466932!3d28.216982482583607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995944d76693d11%3A0xf1f006d89be13640!2sNew+Rd%2C+Pokhara+33700!5e0!3m2!1sen!2snp!4v1480401039246" width="600" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="subsection">
                                 <?php
        if (have_posts()) : while (have_posts()) : the_post();
        ?>
                                    <?php the_content();?>
                                    <?php       
        endwhile; endif;     
        ?>
                                </div>
                            </div>
                            
                        </div>

                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
  <!--Inner page content-->
<?php get_footer();?>
