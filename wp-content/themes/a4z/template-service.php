<?php 
/*
Template Name: Service
*/
get_header();?>
<div class="caption text-center padding-20">
        <h2>Our Services</h2>
        <ol class="breadcrumb pull-right">
          <li><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="active">Our Services</li>
        </ol>
      </div>
    </div>
  </div>
  <!--Inner page content-->
  <section class="our-work padding-20">
    <div class="container">
      <div class="title">
        <h3>Our Services</h3>
        <p class="lead"><?php echo get_the_excerpt(43);?></p>
      </div>
      
      <div class="service-list">
      	<div class="row">
      	 <div class="row">
        <?php
     $args = array(
     'posts_per_page' => -1,
     'post_type' => 'service'              
     );
   $counter = 0;
   query_posts($args);
   global $wp_query;
   $count_post = $wp_query->post_count;
   while (have_posts()) : the_post();
   $counter++;
   ?>
        	<div class="col-md-3">
            	<div class="service-box">
            	<?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'service');
          ?>
                	<div class="image-wrapper"><img src="<?php echo $image[0];?>" alt="<?php the_title();?>"></div>
                	<?php }?>
                    <div class="service-detail">
                    	<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                        <p><?php echo substr(strip_tags(get_the_excerpt()),0,90);?>...</p>
                    </div>
                </div>
            </div>
<?php   endwhile;  
 wp_reset_query();
     ?> 
        </div>
      </div>
    </div>
  </section>
  <!--Inner page content-->

<?php get_footer(); ?>