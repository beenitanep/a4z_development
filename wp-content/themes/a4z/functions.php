<?php 
/*=======================================================================*/
// Wordpress Custom Functions
/*=======================================================================*/

/*=======================================================================*/
// Enqueue Scripts
/*=======================================================================*/


function wpdocs_theme_name_scripts() {
	wp_enqueue_style( 'bootstrap.min', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css',false,'1.1','all');
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.css',false,'1.1','all');
	wp_enqueue_style( 'simple-line-icons', get_template_directory_uri() . '/fonts/simple-line-icons-master/css/simple-line-icons.css',false,'1.1','all');
	wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/plugins/magnific-popup/magnific-popup.css',false,'1.1','all');
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css',false,'1.1','all');
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css',false,'1.1','all');
    wp_enqueue_script( 'jquery-min', get_template_directory_uri() . '/js/jquery.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'bootstrap-min-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'jquery-appear', get_template_directory_uri() . '/js/jquery.appear.js', array(), '1.0.0', true );
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js', array(), '1.0.0', true );
    wp_enqueue_script( 'classie', get_template_directory_uri() . '/js/classie.js', array(), '1.0.0', true );
     wp_enqueue_script( 'search', get_template_directory_uri() . '/js/search.js', array(), '1.0.0', true );
      wp_enqueue_script( 'jquery-parallax', get_template_directory_uri() . '/js/jquery.parallax-1.1.3.js', array(), '1.0.0', true );
      wp_enqueue_script( 'isotope-min', get_template_directory_uri() . '/plugins/isotope/js/isotope.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'sotope-pkgd-min', get_template_directory_uri() . '/plugins/isotope/isotope.pkgd.min.js', array(), '1.0.0', true );
     wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/plugins/magnific-popup/jquery.magnific-popup.min.js', array(), '1.0.0', true );
      wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array(), '1.0.0', true );
	}
	add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

	/*
	=====================================================
	Custom Menus
	=====================================================
*/
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
	register_nav_menus(
		array(
			'primary'   => __('Primary Navigation'),
			'secondary' => __('Secondary Navigation')
		)
	);
} 

 /*=======================================================================*/
// Image Thumbnails (add feature image) 
//post thumbnails enabled
/*=======================================================================*/
add_theme_support( 'post-thumbnails' );

/*=======================================================================*/
// Page excerpt support
/*=======================================================================*/

add_post_type_support('page', 'excerpt');

/*=======================================================================*/
// Image Thumbnails
/*=======================================================================*/
if ( function_exists( 'add_image_size' ) ) { 
add_image_size( 'service', 241, 155,true );
add_image_size( 'about', 555, 229,true );
add_image_size( 'team', 220, 220,true );
  }

  /*=======================================================================*/
// Including Functions Files
/*=======================================================================*/
require_once(TEMPLATEPATH . '/functions/cpt-whyus.php');
require_once(TEMPLATEPATH . '/functions/cpt-work.php');
require_once(TEMPLATEPATH . '/functions/cpt-services.php');
require_once(TEMPLATEPATH . '/functions/cpt-gallery.php');
require_once(TEMPLATEPATH . '/functions/cpt-team.php');
require_once(TEMPLATEPATH . '/functions/customfield.php');
?>

